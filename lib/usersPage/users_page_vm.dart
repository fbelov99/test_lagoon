import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_lagoon/api_service.dart';
import 'package:test_lagoon/models/user_model.dart';

class UsersPageController extends GetxController {
  RxList<Rx<UserModel>> users = [UserModel().obs].obs;
  RxInt page = 1.obs;
  RxBool isBusy = false.obs;
  ScrollController scrollcontroller = ScrollController();

  @override
  void onInit() async {
    users.remove(UserModel().obs);
    await getUsers();
    scrollcontroller.addListener(() {
      if (scrollcontroller.position.pixels ==
          scrollcontroller.position.maxScrollExtent) {
        page++;
        getUsers(loadMore: true);
      }
    });

    super.onInit();
  }

  Future<void> getUsers({bool loadMore = false}) async {
    if (!loadMore) {
      isBusy.update((val) {
        true;
      });
    }
    try {
      await ApiService.instance.getUsersList(page.value).then((value) {
        if (!loadMore) {
          users.value = value.map((e) => e.obs).toList();
        } else {
          users.addAll(value.map((e) => e.obs).toList());
          if (value.isEmpty) {
            page--;
          }
        }
        SharedPreferences.getInstance().then((value) => value.setStringList(
            'users',
            users
                .map((element) => jsonEncode(element.value.toJson()))
                .toList()));
      });
    } catch (e) {
      await SharedPreferences.getInstance().then((value) => users.value = value
              .getStringList('users')
              ?.map((e) => UserModel.fromJson(jsonDecode(e)).obs)
              .toList() ??
          []);
      // showToast(e.toString());
    }
    if (!loadMore) {
      isBusy.update((val) {
        false;
      });
    }
  }
}
