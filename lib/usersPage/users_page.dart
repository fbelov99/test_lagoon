import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_lagoon/userInfoPage/user_info_page.dart';
import 'package:test_lagoon/widget_helper/user_card.dart';

import '../widget_helper/custom_scaffold.dart';
import 'users_page_vm.dart';

class UsersPage extends StatelessWidget {
  const UsersPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetX<UsersPageController>(
        init: UsersPageController(),
        builder: (model) {
          return CustomScaffold(
            isBusy: model.isBusy.value,
            hasData: model.users.isNotEmpty,
            body: Padding(
              padding: EdgeInsets.symmetric(horizontal: Get.width * .03),
              child: ListView.builder(
                  controller: model.scrollcontroller,
                  itemCount: model.users.length,
                  itemBuilder: (context, i) {
                    return Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: Get.height * .005),
                      child: InkWell(
                          onTap: () {
                            Get.to(() => const UserInfoPage(),
                                arguments: model.users[i].value.id);
                          },
                          child: UserCard(user: model.users[i].value)),
                    );
                  }),
            ),
          );
        });
  }
}
