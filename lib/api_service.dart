import 'package:get/get.dart';
import 'package:test_lagoon/models/user_model.dart';

class ApiService extends GetConnect {
  ApiService._privateConstructor();

  static final ApiService _instance = ApiService._privateConstructor();

  static ApiService get instance => _instance;

  Future<List<UserModel>> getUsersList(int page) async {
    try {
      Response response = await get('https://reqres.in/api/users?page=$page');

      if (response.statusCode == 200) {
        return List.of(response.body['data'])
            .map((e) => UserModel.fromJson(e))
            .toList();
      } else {
        throw 'Response code: ${response.statusCode}. ${response.body['error']}';
      }
    } catch (e) {
      throw 'Error. $e';
    }
  }

  Future<UserModel> getUserInfo(int id) async {
    try {
      Response response = await get('https://reqres.in/api/users/$id');

      if (response.statusCode == 200) {
        return UserModel.fromJson(response.body['data']);
      } else {
        throw 'Response code: ${response.statusCode}. ${response.body['error']}';
      }
    } catch (e) {
      throw 'Error. $e';
    }
  }
}
