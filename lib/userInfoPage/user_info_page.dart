import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_lagoon/widget_helper/user_card.dart';

import '../widget_helper/custom_scaffold.dart';
import 'user_info_vm.dart';

class UserInfoPage extends StatelessWidget {
  const UserInfoPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetX<UserInfoController>(
        init: UserInfoController(),
        builder: (model) {
          return CustomScaffold(
            isBusy: model.isBusy.value,
            hasData: model.user.value.id != null,
            body: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: Get.width * .03, vertical: Get.height * .005),
              child: Center(
                child: SizedBox(
                  height: Get.height * .3,
                  child: UserCard(
                    user: model.user.value,
                    isInfo: true,
                  ),
                ),
              ),
            ),
          );
        });
  }
}
