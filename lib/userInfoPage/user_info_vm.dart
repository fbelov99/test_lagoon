import 'dart:convert';

import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_lagoon/api_service.dart';
import 'package:test_lagoon/models/user_model.dart';

class UserInfoController extends GetxController {
  Rx<UserModel> user = UserModel().obs;
  RxInt id = 1.obs;
  RxBool isBusy = false.obs;

  @override
  void onInit() async {
    id.value = Get.arguments;
    await getUserInfo();
    super.onInit();
  }

  Future<void> getUserInfo() async {
    isBusy.update((val) {
      true;
    });
    try {
      await ApiService.instance
          .getUserInfo(id.value)
          .then((value) => user.value = value);
    } catch (e) {
      await SharedPreferences.getInstance().then((value) {
        final saved = value
            .getStringList('users')
            ?.firstWhereOrNull((e) => jsonDecode(e)['id'] == id.value);
        user = UserModel.fromJson(jsonDecode(saved ?? '{}')).obs;
      });
      // showToast(e.toString());
    }
    isBusy.update((val) {
      false;
    });
  }
}
