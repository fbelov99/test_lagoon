import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../models/user_model.dart';

class UserCard extends StatelessWidget {
  final UserModel user;
  final bool isInfo;
  const UserCard({super.key, required this.user, this.isInfo = false});

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(10)),
        padding: EdgeInsets.symmetric(
            horizontal: Get.width * .02, vertical: Get.height * .015),
        child: Column(
          children: [
            if (user.avatar != null)
              ClipRRect(
                borderRadius: BorderRadius.circular(40),
                child: CachedNetworkImage(imageUrl: user.avatar!),
              ),
            if (isInfo) Text('id: ${user.id}'),
            Text('name: ${user.firstName} ${user.lastName}'),
            Text('mail: ${user.email}'),
          ],
        ));
  }
}
