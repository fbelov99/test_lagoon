import 'package:flutter/material.dart';

class CustomScaffold extends StatelessWidget {
  final bool isBusy, hasData;
  final Widget? body;
  const CustomScaffold(
      {super.key, this.isBusy = false, this.hasData = false, this.body});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isBusy
          ? const Center(
              child: CircularProgressIndicator(
                strokeWidth: 4.0,
                color: Colors.blue,
                backgroundColor: Colors.transparent,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
              ),
            )
          : hasData
              ? body
              : const Center(
                  child: Text('No Data'),
                ),
    );
  }
}
